﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FYControls.Controls
{
    ///// <summary>
    ///// timeline 状态栏
    ///// </summary>
    //[TemplatePart(Name = TimelineStatus.TS_STATUS_PART, Type = typeof(FrameworkElement))]
    //[TemplatePart(Name = TimelineStatus.TS_MAIN_GRID_PART, Type = typeof(Grid))]
    //public class TimelineStatus : Control
    //{
       
    //    #region - Events -

    //    public delegate void TimelineStatusEvent(object sender, RoutedEventArgs e);


    //    public event TimelineStatusEvent OnCurrentDatatimeChanged;

    //    #endregion

    //    #region - Private Fields and Constants -

    //    #region Types of Scale

    //    private const string TL_TYPE_DECADES = "decades";
    //    private const string TL_TYPE_YEARS = "years";
    //    private const string TL_TYPE_MONTHS = "months";
    //    private const string TL_TYPE_DAYS = "days";
    //    private const string TL_TYPE_HOURS = "hours";
    //    private const string TL_TYPE_MINUTES = "minutes";
    //    private const string TL_TYPE_MINUTES10 = "minutes10";
    //    private const string TL_TYPE_SECONDS = "seconds";
    //    private const string TL_TYPE_MILLISECONDS100 = "milliseconds100";
    //    private const string TL_TYPE_MILLISECONDS10 = "milliseconds10";
    //    private const string TL_TYPE_MILLISECONDS = "milliseconds";


    //    #endregion

    //    #region - Control Part constants -

    //    private const string TS_MAIN_GRID_PART = "MainGrid";
    //    private const string TS_STATUS_PART = "StatusPart";

    //    #endregion

    //    #region - Template Parts -

    //    public Grid MainGridPart { get; set; }

    //    #endregion

    //    private string m_sourceType = TL_TYPE_YEARS;

    //    private TimelineCalendarType m_timelineType;

    //    private bool m_timeFormat24 = false;

    //    private TimelineBuilder m_calc;

    //    private string m_calendarType;

    //    #endregion

    //    #region - Properties -

    //    private TimelineTray _tray;

    //    /// <summary>
    //    /// timelinestatus所在的timelinetray主容器
    //    /// </summary>
    //    public TimelineTray TimelineTray
    //    {
    //        get
    //        {
    //            if (_tray == null && Parent as TimelineTray != null)
    //            {
    //                _tray = Parent as TimelineTray;
    //            }
    //            return _tray;
    //        }

    //        set
    //        {
    //            TimelineTray old = _tray;
    //            _tray = value;
    //            OnTimelineTrayChanged(old, _tray);
    //        }
    //    }

    //    protected virtual void OnTimelineTrayChanged(
    //        TimelineTray old,
    //        TimelineTray newTray
    //    )
    //    {
    //    }

    //    public TimelineCalendar ItemsSource
    //    {
    //        get;
    //        set;
    //    }

    //    public string ItemSourceType
    //    {
    //        get
    //        {
    //            return m_sourceType;
    //        }

    //        set
    //        {
    //            string itemsType;

    //            itemsType = value.ToLower();
    //            m_sourceType = value;

    //            switch (itemsType)
    //            {
    //                case TL_TYPE_DECADES:
    //                    m_timelineType = TimelineCalendarType.Decades;
    //                    break;

    //                case TL_TYPE_YEARS:
    //                    m_timelineType = TimelineCalendarType.Years;
    //                    break;

    //                case TL_TYPE_MONTHS:
    //                    m_timelineType = TimelineCalendarType.Months;
    //                    break;

    //                case TL_TYPE_DAYS:
    //                    m_timelineType = TimelineCalendarType.Days;
    //                    break;

    //                case TL_TYPE_HOURS:
    //                    m_timelineType = TimelineCalendarType.Hours;
    //                    break;

    //                case TL_TYPE_MINUTES10:
    //                    m_timelineType = TimelineCalendarType.Minutes10;
    //                    break;

    //                case TL_TYPE_MINUTES:
    //                    m_timelineType = TimelineCalendarType.Minutes;
    //                    break;

    //                case TL_TYPE_SECONDS:
    //                    m_timelineType = TimelineCalendarType.Seconds;
    //                    break;

    //                case TL_TYPE_MILLISECONDS100:
    //                    m_timelineType = TimelineCalendarType.Milliseconds100;
    //                    break;

    //                case TL_TYPE_MILLISECONDS10:
    //                    m_timelineType = TimelineCalendarType.Milliseconds10;
    //                    break;

    //                case TL_TYPE_MILLISECONDS:
    //                    m_timelineType = TimelineCalendarType.Milliseconds;
    //                    break;

    //                default:
    //                    throw new ArgumentOutOfRangeException();
    //            }
    //        }
    //    }

    //    public bool TimeFormat24
    //    {
    //        get
    //        {
    //            return m_timeFormat24;
    //        }
    //        set
    //        {
    //            m_timeFormat24 = value;
    //            if (m_calc != null && m_calc.Calendar != null)
    //            {
    //                m_calc.Calendar.TimeFormat24 = m_timeFormat24;
    //            }
    //        }
    //    }

    //    #endregion

    //    #region - Template -

    //    #region - DefaultStatusItemTemplate -

    //    public static readonly DependencyProperty DefaultStatusItemTemplateProperty =
    //            DependencyProperty.Register("DefaultStatusItemTemplate",
    //            typeof(DataTemplate),
    //            typeof(TimelineStatus),
    //    new PropertyMetadata(DefaultStatusItemTemplateChanged));


    //    public static void DefaultStatusItemTemplateChanged(
    //        DependencyObject d,
    //        DependencyPropertyChangedEventArgs e
    //    )
    //    {
    //        TimelineStatus band;
    //        band = (TimelineStatus)d;

    //        if (band != null)
    //        {
    //            band.OnDefaultStatusItemTemplateChanged(e);
    //        }
    //    }

    //    protected virtual void OnDefaultStatusItemTemplateChanged(
    //        DependencyPropertyChangedEventArgs e
    //    )
    //    {
    //        DefaultStatusItemTemplate = (DataTemplate)e.NewValue;
    //    }

    //    public DataTemplate DefaultStatusItemTemplate
    //    {
    //        get
    //        {
    //            return (DataTemplate)GetValue(DefaultStatusItemTemplateProperty);
    //        }

    //        set
    //        {
    //            SetValue(DefaultStatusItemTemplateProperty, value);
    //        }
    //    }

    //    #endregion

    //    #region - StatusItemTemplate -

    //    public static readonly DependencyProperty StatusItemTemplateProperty =
    //            DependencyProperty.Register("StatusItemTemplate",
    //            typeof(DataTemplate),
    //            typeof(TimelineStatus),
    //    new PropertyMetadata(StatusItemTemplateChanged));


    //    public static void StatusItemTemplateChanged(
    //        DependencyObject d,
    //        DependencyPropertyChangedEventArgs e
    //    )
    //    {
    //        TimelineStatus band;
    //        band = (TimelineStatus)d;

    //        if (band != null)
    //        {
    //            band.OnStatusItemTemplateChanged(e);
    //        }
    //    }

    //    protected virtual void OnStatusItemTemplateChanged(
    //        DependencyPropertyChangedEventArgs e
    //    )
    //    {
    //        StatusItemTemplate = (DataTemplate)e.NewValue;
    //    }

    //    public DataTemplate StatusItemTemplate
    //    {
    //        get
    //        {
    //            return (DataTemplate)GetValue(StatusItemTemplateProperty);
    //        }

    //        set
    //        {
    //            SetValue(StatusItemTemplateProperty, value);
    //        }
    //    }

    //    #endregion

    //    #endregion

    //    static TimelineStatus()
    //    {
    //        DefaultStyleKeyProperty.OverrideMetadata(typeof(TimelineStatus),
    //            new FrameworkPropertyMetadata(typeof(TimelineStatus)));
    //    }

    //    public override void OnApplyTemplate()
    //    {
    //        base.OnApplyTemplate();
    //        MainGridPart = (Grid)GetTemplateChild(TS_MAIN_GRID_PART);
    //    }

    //    public void CreateTimelineStatusCalculator(
    //        string calendarType,
    //        DateTime currentDateTime,
    //        DateTime minDateTime,
    //        DateTime maxDateTime
    //    )
    //    {
    //        Debug.Assert(this.TimelineTray != null);
    //        ItemsSource = new TimelineCalendar(calendarType, m_timelineType, minDateTime, maxDateTime);
    //        ItemsSource.TimeFormat24 = TimeFormat24;

    //        if(StatusItemTemplate == null)
    //        {
    //            StatusItemTemplate = DefaultStatusItemTemplate;
    //        }

    //        //m_calc = new TimelineBuilder()

    //    }
    //}

    /// <summary>
    /// timeline 状态栏块
    /// </summary>
    public class TimelineStatusItem: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void FirePropertyChanged(
            string name
        )
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        string m_statusKey;
        public string StatusKey
        {
            get
            {
                return m_statusKey;
            }
            set
            {
                m_statusKey = value;
                FirePropertyChanged("StatusKey");
            }
        }

        string m_statusName;
        public string StatusName
        {
            get
            {
                return m_statusName;
            }
            set
            {
                m_statusName = value;
                FirePropertyChanged("StatusName");
            }
        }

        DateTime m_startTime;
        public DateTime StartTime
        {
            get
            {
                return m_startTime;
            }
            set
            {
                m_startTime = value;
                FirePropertyChanged("StartTime");
            }
        }

        DateTime m_endTime;
        public DateTime EndTime
        {
            get
            {
                return m_endTime;
            }
            set
            {
                m_endTime = value;
                FirePropertyChanged("EndTime");
            }
        }

        string m_statusColor;
        public string StatusColor
        {
            get
            {
                return m_statusColor;
            }
            set
            {
                m_statusColor = value;
                FirePropertyChanged("StatusColor");
            }
        }
    }

    public class TimelineStatusItemDisplay : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        protected void FirePropertyChanged(
            string name
        )
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private TimelineBand m_band;
        private TimelineBuilder m_builder;
        private TimelineStatusItem m_item;

        public TimelineStatusItemDisplay(
            TimelineStatusItem item,
            TimelineBand band,
            TimelineBuilder builder
        )
        {
            m_item = item;
            m_band = band;
            m_builder = builder;
            StatusPixelWidth = TimelineBuilder.TimeSpanToPixels(m_item.EndTime - m_item.StartTime);
            StatusPixelHeight = m_band.StatusHeight;
            StatusColor = m_item.StatusColor;
            this.PropertyChanged += TimelineStatusDisplayItem_PropertyChanged;
        }

        private void TimelineStatusDisplayItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "StartTime")
            {

                //StatusPixelWidth = Math.Max(3, TimelineBuilder.TimeSpanToPixels(this.StatusItem.EndTime - this.StatusItem.StartTime));
                //FirePropertyChanged("StatusPixelWidth");
            }
            else if (e.PropertyName == "EndTime")
            {
                //StatusPixelWidth = Math.Max(3, TimelineBuilder.TimeSpanToPixels(this.StatusItem.EndTime - this.StatusItem.StartTime));
                //FirePropertyChanged("StatusPixelWidth");
            }
        }

        public TimelineBand TimelineBand
        {
            get => m_band;
            set => m_band = value;
        }

        public TimelineBuilder TimelineBuilder
        {
            get => m_builder;
            set => m_builder = value;
        }

        public TimelineStatusItem StatusItem
        {
            get => m_item;
            set => m_item = value;
        }

        private double m_statusPixelWidth;
        public double StatusPixelWidth
        {
            get => m_statusPixelWidth;
            set { m_statusPixelWidth = value; FirePropertyChanged("StatusPixelWidth"); }
        }

        private double m_statusPixelLeft;
        public double StatusPixelLeft
        {
            get => m_statusPixelLeft;
            set { m_statusPixelLeft = value; FirePropertyChanged("StatusPixelLeft"); }
        }

        private double m_statusPixelHeight;


        public double StatusPixelHeight
        {
            get => m_statusPixelHeight;
            set { m_statusPixelHeight = value; FirePropertyChanged("StatusPixelHeight"); }
        }

        string m_statusColor;
        public string StatusColor
        {
            get
            {
                return m_statusColor;
            }
            set
            {
                m_statusColor = value;
                FirePropertyChanged("StatusColor");
            }
        }

        public void Recalculate()
        {
            StatusPixelWidth = Math.Max(3, TimelineBuilder.TimeSpanToPixels(this.StatusItem.EndTime - this.StatusItem.StartTime));
            FirePropertyChanged("StatusPixelWidth");
        }
    }
}
