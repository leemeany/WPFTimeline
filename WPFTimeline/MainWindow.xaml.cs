﻿using FYControls.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFTimeline
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window,INotifyPropertyChanged
    {
        public double StatusHeight { get; set; } = 30;


        private ObservableCollection<TimelineStatusItem> m_statusList = new ObservableCollection<TimelineStatusItem>();
        public ObservableCollection<TimelineStatusItem> StatusList
        {
            get
            {
                return m_statusList;
            }
            set
            {
                m_statusList = value;
                FirePropertyChanged("StatusList");
            }
        }
            


        public MainWindow()
        {
            TimelineStatusItem status1 = new TimelineStatusItem
            {
                StatusKey = Guid.NewGuid().ToString(),
                StatusName = "status1",
                StartTime = new DateTime(2018, 12, 12, 7, 0, 0),
                EndTime = new DateTime(2018, 12, 12, 8, 0, 0),
                StatusColor = "#00FF00"
            };

            TimelineStatusItem status2 = new TimelineStatusItem
            {
                StatusKey = Guid.NewGuid().ToString(),
                StatusName = "status2",
                StartTime = new DateTime(2018, 12, 12, 8, 0, 0),
                EndTime = new DateTime(2018, 12, 12, 9, 0, 0),
                StatusColor = "#CD5555"
            };

            StatusList.Add(status1);
            StatusList.Add(status2);

            this.DataContext = this;
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void FirePropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void Timeline_TimelineReady(object sender, EventArgs e)
        {

            TimelineEvent Event1 = new TimelineEvent
            {
                Id = Guid.NewGuid().ToString(),
                Description = "引导",
                StartDate = new DateTime(2018, 12, 12, 7, 10, 0),
                EndDate = new DateTime(2018, 12, 12, 7, 40, 0),
                IsDuration = true
            };

            TimelineEvent Event2 = new TimelineEvent
            {
                Id = Guid.NewGuid().ToString(),
                Description = "推出",
                StartDate = new DateTime(2018, 12, 12, 8, 0, 0),
                EndDate = new DateTime(2018, 12, 12, 8, 15, 0),
                EventColor = "Red",
                IsDuration = true

            };

            TimelineEvent Event3 = new TimelineEvent
            {
                Id = Guid.NewGuid().ToString(),
                Description = "推出",
                StartDate = new DateTime(2018, 12, 12, 8, 10, 0),
                EndDate = new DateTime(2018, 12, 12, 8, 30, 0),
                EventColor = "Blue",
                IsDuration = true

            };
            timeline.TimelineEvents.Add(Event1);
            timeline.TimelineEvents.Add(Event2);
            timeline.TimelineEvents.Add(Event3);
            timeline.ResetEvents(timeline.TimelineEvents);
        }
    }
}
