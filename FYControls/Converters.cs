﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FYControls
{
    public class GridHeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var height = (double)value;
                return new GridLength(height);
            }
            else
                return new GridLength(0.0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var height = (GridLength)value;
                return height.Value;
            }
            else
                return 0.0;
        }
    }

    public class ColorToBrushConverter : IValueConverter
    {
        private System.Windows.Media.Color ConvertColorFromString(string str)
        {
            var color = System.Drawing.ColorTranslator.FromHtml(str);
            return System.Windows.Media.Color.FromRgb(color.R, color.G, color.B);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new System.Windows.Media.SolidColorBrush(ConvertColorFromString(value as string)); 
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DateTimeToStringConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var time = (DateTime)value;
            return string.Format("{0:t}", time);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
