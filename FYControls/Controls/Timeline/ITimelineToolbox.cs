﻿using System;

namespace FYControls.Controls
{
    /// 
    /// <summary>
    /// Interface which is implemented by TimelineTray to be used by TimelineToolbox</summary>
    /// 
    public interface ITimelineToolboxTarget
    {
        void FindMinDate();
        void FindMaxDate();
        void FindDate(DateTime date);
        void MoveLeft();
        void MoveRight();
        void ZoomIn();
        void ZoomOut();
    }

    /// 
    /// <summary>
    /// Interface which should be implemented by a toolbox</summary>
    /// 
    public interface ITimelineToolbox
    {
        void SetSite(ITimelineToolboxTarget target);
    }
}
