﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace FYControls.Controls
{
    /// 
    /// <summary>
    /// Uri data for timeline. Data sould be all in //data/event format</summary>
    /// 
    public class UriInfo
    {
        public Uri Url
        {
            get;
            set;
        }

        public Uri IconUrl
        {
            get;
            set;
        }

        public Color EventColor
        {
            get;
            set;
        }
    }

    /// 
    /// <summary>
    /// List of urls to xml load data from. Data sould be all in //data/event format </summary>
    /// 
    public class TimelineUrlCollection : ObservableCollection<UriInfo>
    {
    }
}